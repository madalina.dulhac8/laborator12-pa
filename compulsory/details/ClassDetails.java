package compulsory.details;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class ClassDetails {
    Class<?> Class;

    public ClassDetails(java.lang.Class<?> myClass) {
        Class = myClass;
    }

    public void classInfo() {

        System.out.println("Class name is: " + this.Class.getName());
        System.out.println("Class package is: " + this.Class.getPackage());
        Method[] methods = this.Class.getDeclaredMethods();
        for (Method m : methods) {
            System.out.println("Method name: " + m.getName());
            this.showModifiers(m);
        }

    }

    public void showModifiers(Method method) {
        int modifiers = method.getModifiers();
        if (Modifier.isPublic(modifiers)) System.out.println("Public method");
        if (Modifier.isPrivate(modifiers)) System.out.println("Private method");
        if (Modifier.isProtected(modifiers)) System.out.println("Protected method");
        if (Modifier.isAbstract(modifiers)) System.out.println("Abstract method");
        if (Modifier.isFinal(modifiers)) System.out.println("Final method");
        if (Modifier.isStatic(modifiers)) System.out.println("Static method");
    }

    public void callStaticMethods() {
        Method[] methods = this.Class.getDeclaredMethods();
        for(Method method : methods) {
            method.setAccessible(true); //if the method is private, I am able to call it
            Annotation[] annotations = method.getAnnotations();
            for(Annotation annotation : annotations) {
                Class<? extends Annotation> typo = annotation.annotationType();
                if(typo.getSimpleName().equals("Test")) {
                    if(method.getParameterCount() == 0 && Modifier.isStatic(method.getModifiers())) {
                        try {
                            method.invoke(this.Class);
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

}
