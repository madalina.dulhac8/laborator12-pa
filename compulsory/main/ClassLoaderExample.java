package compulsory.main;

import compulsory.details.ClassDetails;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

public class ClassLoaderExample {

    public static void main (String[] args) {
        File file = new File("C:\\Users\\Wks\\Desktop\\PA_Lab12\\src\\main\\java\\");

        try {
            URL url = file.toURI().toURL();
            URL[] urls = new URL[]{url};
            ClassLoader cl = new URLClassLoader(urls);

            Class cls = cl.loadClass("compulsory.myclass.Person");
            ClassDetails details = new ClassDetails(cls);
            details.classInfo();
            details.callStaticMethods();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
