# 12th laboratory for the Advanced Programming Course

## Compulsory

* Create an application to analyze and test java classes.
The application will receive as input java classes and it will display their prototypes and perform the tests specified by the @Test annotation.

* The main specifications of the application are:

* The input will be a .class file, located anywhere in the file system.
* Load the specified class in memory, identifying dynamically its package.
* Using reflection, extract as many information about the class (at least its methods).
* Using reflection, invoke the static methods, with no arguments, annotated with @Test.


##Solution Compulsory
* I created three packages for a better organization.
* In details package, I created a class responsible for detailing the specifications of a .class file (name, package, methods).
Furthermore, I implemented a method in order to call all the static methods, with no arguments, annotated with @Test.
* The myclass package contains a simple java class, named Person, which I used to illustrate the way my program works.
* In main I loaded the class from a specific path and I called the methods from ClassDetails. Fun things ahead
