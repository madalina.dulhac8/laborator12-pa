package bonus;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

/**
 * Class Compiler tries to compile the .java files at a certain path
 */
public class Compiler {


    /**
     *
     * The method receives a file based on which it attempts to generate class files with the command
     * "javac path/*.java"
     */
    public static void compilePath(File file){


        try {
            String path=file.getAbsolutePath();
            if(path.contains(".java")) {
                int value = path.lastIndexOf("\\");
                path = path.substring(0, value + 1);
            }else path=path+"\\";
            String cmd = "javac "+path+"*.java" ;
            Runtime run = Runtime.getRuntime();
            System.out.println(cmd);
            Process pr = run.exec(cmd);
            pr.waitFor();
            BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
            String line = "";
            while ((line = buf.readLine()) != null) {
                System.out.println(line);
            }
        }catch (Exception e){System.out.println(e.toString());}
    }
}
