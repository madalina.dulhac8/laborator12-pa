package walk;

import compulsory.details.ClassDetails;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class Load {
    File directory;

    public Load(File classFile) {

        this.directory = classFile;
    }


    public void load(File file) {

        Path path = new Path(file);
        Class<?> cls = path.load();
        ClassDetails details = new ClassDetails(cls);
        details.classInfo();
        details.callStaticMethods();

    }


    public void walkThroughDirectory(File root) {

        File[] list = root.listFiles();

        if (list == null) return;

        for (File f : list) {
            if (f.isDirectory()) {
                walkThroughDirectory(f);
            } else {
                if (f.getAbsolutePath().endsWith(".class"))
                    this.load(f);
            }
        }
    }


    public void loadJar(File root) {
        try {
            JarFile jarFile = new JarFile(root.getAbsolutePath());
            Enumeration<JarEntry> e = jarFile.entries();

            URL[] urls = {new URL("jar:file:" + root.getAbsolutePath() + "!/")};
            URLClassLoader cl = URLClassLoader.newInstance(urls);

            while (e.hasMoreElements()) {
                JarEntry je = e.nextElement();
                if (je.isDirectory() || !je.getName().endsWith(".class")) {
                    continue;
                }

                String className = je.getName().substring(0, je.getName().length() - 6);
                className = className.replace('/', '.');
                Class classs = cl.loadClass(className);
                ClassDetails details = new ClassDetails(classs);
                details.classInfo();
                //detailer.callStaticTest();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
