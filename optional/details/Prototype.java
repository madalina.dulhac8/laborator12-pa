package compulsory.details;

import walk.Path;

import java.io.File;
import java.lang.reflect.Field;

public class Prototype {
    private File classFile;
    private Class cls;

    public Prototype(File classFile) {
        this.classFile = classFile;
        Path loader = new Path(classFile);
        this.cls = loader.load();
    }

    /**
     * The method prints what the class should look like based on the .class file
     */
    public void execute() {

        System.out.print(cls.getName().substring(cls.getName().indexOf(".") + 1));
        System.out.print(" extends ");
        System.out.print(cls.getSuperclass().getName().substring(cls.getSuperclass().getName().indexOf(".") + 1));
        System.out.println("");
        Field[] fields = cls.getDeclaredFields();
        for (Field f : fields) {

            System.out.print(f.getType().getName()+" ");
            System.out.println(f.getName());
        }
        ClassDetails.ShowMethod(this.cls);
    }
}

