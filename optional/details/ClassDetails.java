package compulsory.details;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class ClassDetails {
    Class<?> Class;

    public ClassDetails(java.lang.Class<?> myClass) {
        Class = myClass;
    }

    public void classInfo() {

        System.out.println("Class name is: " + this.Class.getName());
        System.out.println("Class package is: " + this.Class.getPackage());
        Method[] methods = this.Class.getDeclaredMethods();
        for (Method m : methods) {
            System.out.println("Method name: " + m.getName());
            this.showModifiers(m);
        }

    }


    public static void showModifiers(Method method) {
        int modifiers = method.getModifiers();
        if (Modifier.isPublic(modifiers)) System.out.println("Public method");
        if (Modifier.isPrivate(modifiers)) System.out.println("Private method");
        if (Modifier.isProtected(modifiers)) System.out.println("Protected method");
        if (Modifier.isAbstract(modifiers)) System.out.println("Abstract method");
        if (Modifier.isFinal(modifiers)) System.out.println("Final method");
        if (Modifier.isStatic(modifiers)) System.out.println("Static method");
    }

    public static void ShowMethod(Class cls) {
        Method[] methods = cls.getDeclaredMethods();
        for (Method m : methods) {
            ClassDetails.showModifiers(m);
            System.out.print(m.getName() + " ( ");
            Class[] param = m.getParameterTypes();
            for (Class c : param) {
                System.out.print(c.getName() + " , ");
            }
            System.out.println(" ) ");
        }
    }

    public void callStaticMethods() {
        Method[] methods = this.Class.getDeclaredMethods();
        for(Method method : methods) {
            method.setAccessible(true); //if the method is private, I am able to call it
            Annotation[] annotations = method.getAnnotations();
            for(Annotation annotation : annotations) {
                Class<? extends Annotation> typo = annotation.annotationType();
                if(typo.getSimpleName().equals("Test")) {
                    if(method.getParameterCount() == 0 && Modifier.isStatic(method.getModifiers())) {
                        try {
                            method.invoke(this.Class);
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    public static boolean isMethodTest(Method m) {
        Annotation[] annotations = m.getAnnotations();
        for (Annotation a : annotations) {
            Class<? extends Annotation> type = a.annotationType();
            if (type.getSimpleName().equals("Test")) return true;
        }
        return false;
    }

    public void callTest(Class cls) {
        Method[] methods = cls.getDeclaredMethods();
        int value = 0;
        int argumentsTotal=0;
        int methodsArguments=0;
        try {
            for (Method m : methods) {
                m.setAccessible(true);
                if (ClassDetails.isMethodTest(m)) {
                    Class[] param = m.getParameterTypes();
                    Object[] generated = new Object[m.getParameterCount()];
                    if(m.getParameterCount() > 0)
                        methodsArguments += 1;
                    int count = 0;
                    for (Class p : param) {
                        if (p.getName().contains("String"))
                            generated[count] = "test";
                        else
                            generated[count] = 0;
                        count++;
                    }
                    argumentsTotal+=count;
                    System.out.println("Calling method \"" + m.getName() + "\"");
                    m.invoke(m.getClass(), generated);
                    value++;
                }
            }
            System.out.println("Finished calling @Test methods");
            System.out.println("Found " + value + " methods");
            System.out.println("Methods with arguments: "+methodsArguments);
            System.out.println("Total generated arguments: "+argumentsTotal);
        } catch (Exception e) {
            System.out.println(e);
        }


    }

}
